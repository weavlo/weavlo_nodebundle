#!/usr/bin/env node
// This script just handles the CLI portion of the program.

"use strict";
import chalk from "chalk";    // https://www.npmjs.com/package/chalk
const { red } = chalk;
import commander from "commander";
const { Command } = commander;
import { Controller } from "../lib/controller.js";

// Instantiate new Commander CLI
const program = new Command();

program
    .version("1.0.0", "-v, --version")
    .usage("[OPTIONS]...")
    .requiredOption("-b, --benchdir <path>", "Path to a Bench instance.")
    .option("--app <app_name>", "Name of a specific app to bundle.")
    .option("-d, --debug", "output extra debugging", false)
    .parse(process.argv);

if (program.debug) {
    console.log(program.opts());
}

// Using try/catch to simplify CLI output for users.
try {
    Controller.main(program.benchdir, program.app_name);
}
catch(err) {
    console.log(red(err));
    if (program.debug) {
        throw(err);
    }
}
