// lib/options.js
import fs from "fs";
import lodash from "lodash";
import path from "path";
import { readBuildfile_byPath } from "./config.js";

// npmjs.com
// import multi_entry from "@rollup/plugin-multi-entry";
import buble from "@rollup/plugin-buble";
import commonjs from "@rollup/plugin-commonjs";
import less_loader from "less-loader";
import nodeResolve from "@rollup/plugin-node-resolve";
import postcss from "rollup-plugin-postcss";
import node_resolve from "rollup-plugin-node-resolve";
// Local
import { frappe_html } from "./frappe-html-plugin.js";

export class optionBuilder {
    constructor(_configData, _targetApp) {
        if ( _configData == undefined) {
            throw("ArgumentMissing: Call to class optionBuilder with undefined argument '_configData'");
        }
        if ( _targetApp == undefined) {
            throw("ArgumentMissing: Call to class optionBuilder with undefined argument '_targetApp'");
        }
        this.configData = _configData;
        this.targetApp = _targetApp;
        this.appPaths = lodash.map(this.configData.apps, 'path');
    }

    static get_options_for_app(_configData, _targetApp) {
        // Entry point into our class.            
        return new optionBuilder(_configData, _targetApp).buildAppOptions();
    }

    buildAppOptions() {
        const appData = lodash.find(this.configData.apps, { "name": this.targetApp } );
        const obj_build_bundle = readBuildfile_byPath(appData.build_data_path)["bundle"];

        if (!obj_build_bundle) {
            return [];
        }

        return Object.keys(obj_build_bundle)
            .map(output_file => {
                const input_files = obj_build_bundle[output_file]
                    .map(input_file => {
                        let prefix = appData.path;
                        if (input_file.startsWith("node_modules/")) {
                            prefix = this.configData.bench_path; // node_modules is beneath a bench.
                        }
                        return path.resolve(prefix, input_file);
                    });
                return Object.assign(
                    this.get_rollup_options(output_file, input_files), {
                        output_file
                    });
            })
            .filter(Boolean);
    }

    get_rollup_options(output_file, input_files) {
        // Calls 1 of 2 functions, depending on output_file extension.
        if (output_file.endsWith(".css")) {
            return this.get_rollup_options_for_css(output_file, input_files);
        } else if(output_file.endsWith(".js")) {
            return this.get_rollup_options_for_js(output_file, input_files);
        } else {
            throw new Error(chalk.red(`Cannot determine Rollup options for file '${output_file}'`));
        }
    }

    get_rollup_options_for_css(output_file, input_files) {
        const output_path = path.resolve(this.configData.assets_path, output_file);
        const minimize_css = output_path.startsWith("css/") && this.configData.is_production;
        const frappeAppData = lodash.find(this.configData.apps, { "name": "frappe" } );

        const plugins = [
            // enables array of inputs
            //multi_entry(),
            // less -> css
            postcss({
                extract: output_path,
                //loaders: [less_loader],
                use: [
                    ["less", {
                        // import other less/css files starting from these folders
                        paths: [
                            path.resolve(frappeAppData.path, "public", "less")
                        ]
                    }],
                    ["sass", this.get_options_for_scss()]
                ],
                include: [
                    path.resolve(this.configData.bench_path, "**/*.less"),
                    path.resolve(this.configData.bench_path, "**/*.scss"),
                    path.resolve(this.configData.bench_path, "**/*.css")
                ],
                minimize: minimize_css,
                sourceMap: output_file.startsWith("css/") && !this.configData.is_production
            })
        ];

        return {
            inputOptions: {
                input: input_files,
                plugins: plugins,
                onwarn(warning) {
                    // skip warnings
                    if (["EMPTY_BUNDLE"].includes(warning.code)) return;

                    // console.warn everything else
                    log(chalk.yellow.underline(warning.code), ":", warning.message);
                }
            },
            outputOptions: {
                // this file is always empty, remove it later?
                file: path.resolve(this.configData.assets_path, "css/rollup.manifest.css"),
                format: "cjs"
            }
        };
    }

    get_rollup_options_for_js(output_file, input_files) {
        //const apps_list = lodash.map(this.configData.apps, 'name');
        const app_paths =  lodash.map(this.configData.apps, 'path');
        const node_resolve_paths = [].concat(
            // node_modules of apps directly importable
            app_paths.map(app_path => path.resolve(app_path,  "../node_modules")).filter(fs.existsSync),
            //apps_list.map(app => path.resolve(get_app_path(app), "../node_modules")).filter(fs.existsSync),

            // import js file of any app if you provide the full path
            //apps_list.map(app => path.resolve(get_app_path(app), "..")).filter(fs.existsSync)
            app_paths.map(app_path => path.resolve(app_path,  "..")).filter(fs.existsSync)

        );

        const plugins = [
            // enables array of inputs
            //multi_entry(),
            // .html -> .js
            frappe_html(),
            // ignore css imports
            this.ignore_css(),
            // .vue -> .js
            //vue.default(),
            // ES6 -> ES5
            buble({
                objectAssign: "Object.assign",
                transforms: {
                    dangerousForOf: true,
                    classes: false
                },
                exclude: [
                    path.resolve(this.configData.bench_path, "**/*.css"),
                    path.resolve(this.configData.bench_path, "**/*.less")
                ]
            }),
            commonjs(),
            node_resolve({
                customResolveOptions: {
                    paths: node_resolve_paths
                }
            }),
            this.configData.is_production && terser()
        ];

        // Only for Version 12+, add Vue as an additional plugin.
        const frappeAppData = lodash.find(this.configData.apps, { "name": "frappe" } );
        if (frappeAppData.semantic_version.major >= 12 ) {
            import('rollup-plugin-vue')
            .then(() => {
                plugins.push(vue.default());
            });
        }

        return {
            inputOptions: {
                input: input_files,
                plugins: plugins,
                context: "window",
                external: ["jquery"],
                onwarn({ code, message, loc, frame }) {
                    // skip warnings
                    if (["EVAL", "SOURCEMAP_BROKEN", "NAMESPACE_CONFLICT"].includes(code)) return;

                    if ("UNRESOLVED_IMPORT" === code) {
                        log(chalk.yellow.underline(code), ":", message);
                        const command = chalk.yellow("bench setup requirements");
                        log(`Cannot find some dependencies. You may have to run "${command}" to install them.`);
                        log();
                        return;
                    }

                    if (loc) {
                        log(`${loc.file} (${loc.line}:${loc.column}) ${message}`);
                        if (frame) log(frame);
                    } else {
                        log(chalk.yellow.underline(code), ":", message);
                    }
                }
            },
            outputOptions: {
                file: path.resolve(this.configData.assets_path, output_file),
                format: "iife",
                name: "Rollup",
                globals: {
                    "jquery": "window.jQuery"
                },
                sourcemap: true
            }
        };
    }

    get_options_for_scss() {
        const node_modules_path = path.resolve(this.configData.bench_path, "node_modules");
        return {
            includePaths: [
                node_modules_path,
                ...this.appPaths
            ]
        };
    }

    ignore_css() {
        // Function referenced by get_rollup_options_for_js()
        return {
            name: "ignore-css",
            transform(code, id) {
                if (![".css", ".scss", ".sass", ".less"].some(ext => id.endsWith(ext))) {
                    return null;
                }
                return `
                    // ignored ${id}
                `;
            }
        };
    }

} // end of class 'options'