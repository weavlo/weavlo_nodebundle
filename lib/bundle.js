// lib/bundle.js
import chalk from "chalk";
import fs from "fs";
import lodash from "lodash";
import path from "path";
import rollup from "rollup";  // Version ^1.29.1 does not offer 'named exports';
import { optionBuilder } from "./options.js";
const log = console.log; // eslint-disable-line

export class bundler{
    constructor(_configData, targetApp) {
        this.configData = _configData;
        this.targetApp = targetApp;
    }

    static bundleMultiApp(_configData) {

        function run_serially(tasks) {
            let result = Promise.resolve();
            tasks.forEach(task => {
                if(task) {
                    result = result.then ? result.then(task) : Promise.resolve();
                }
            });
            return result;
        }
    
        run_serially(
            lodash.forEach(lodash.map(_configData.apps, "name"), function(appname) {
                //new bundler(appname, this.configData).bundleApp(_appName);
                bundler.bundleSingleApp(_configData, appname);
            })
        );
    }

    static bundleSingleApp(_configData, _targetApp) {
        console.log(`\u2022 Bundling assets for App '${_targetApp}'...`);
        let this_bundler = new bundler(_configData, _targetApp);
        this_bundler.build_assets_for_app(_targetApp)
            .then(() => {
                this_bundler.run_build_command_for_app(_targetApp);
            });        
    }

    build_assets_for_app() {
        // This is the 1st function called.
        const options = optionBuilder.get_options_for_app(this.configData, this.targetApp);
        if (!options.length) {
            return Promise.resolve();
        }            

        const promises = options.map(({ inputOptions, outputOptions, output_file}) => {
            return this.rollup_to_bundle(inputOptions, outputOptions)
                .then(() => {
                    log(`${chalk.green("✔")} Built ${output_file}`);
                });
        });
    
        const start = Date.now();
        return Promise.all(promises)
            .then(() => {
                const time = Date.now() - start;
                log(chalk.green(`✨  Done in ${time / 1000}s`));
            });
    } // EOF: build_assets_for_app
    
    
    // Subfunction of build_assets_for_app
    rollup_to_bundle(inputOptions, outputOptions) {
        return rollup.rollup(inputOptions)
            .then(bundle => bundle.write(outputOptions))
            .catch(err => {
                log(chalk.red(err));
                // Kill process to fail in a CI environment
                if (process.env.CI) {
                    process.kill(process.pid);
                }
            });
    }
    
    
    run_build_command_for_app(app) {
        // This is the 2nd function called.
        if (app === "frappe") {
            return;
        }
        const frappeData = lodash.find(this.configData.apps, { "name": "frappe" } );
        let package_json_path = path.resolve(frappeData.repo_path, "package.json");

        if (fs.existsSync(package_json_path)) {
            let packageJson = require(package_json_path);
            if (packageJson.scripts && packageJson.scripts.build) {
                console.log("\nRunning build command for", chalk.bold(app));
                process.chdir(frappeData.repo_path);
                fs.execSync("yarn build", { encoding: "utf8", stdio: "inherit" });
            }
        }
    }
} // end class 'bundler'


/* ---------------------------------------------------------------------- */

/*

*/

// const postcss = require("rollup-plugin-postcss");


//TODO:  Read the version number from apps/frappe/__init__.py
//       Add these variables, depending on version.
// const { terser } = require('rollup-plugin-terser');
// const vue = require("rollup-plugin-vue");

// const frappe_html = require("./frappe-html-plugin");
