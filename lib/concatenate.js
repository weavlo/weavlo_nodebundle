// lib/concatenate.js
import { readFileSync, writeFileSync } from "fs";
import { resolve as path_resolve} from "path";
// ES6 Modules still experimental; 2 lines needed for Chalk.
// Node doesn't support importing named exports from a CommonJS module 
import chalk  from "chalk";
const { green } = chalk;
import lodash from "lodash";
import { readBuildfile_byPath } from "./config.js";

export function concatenateFrappeFiles(_configData) {
    if ( _configData == undefined) {
        throw("ArgumentMissing: Call to class Concatenate with undefined argument '_configData'");
    }
    // See README.md for function explanation.
    console.log("\u2022 Concatenating JS assets for App: 'frappe'");

    const frappeData = lodash.find(_configData.apps, { "name": "frappe" } );
    const obj_build_concat = readBuildfile_byPath(frappeData.build_data_path)["concat"];

    Object.entries(obj_build_concat).map(([output_file, input_files]) => {
        const file_content = input_files.map(file_name => {
            let full_path = "";
            if (file_name.startsWith("node_modules/")) {
                // This is correct for NPM, but yarn may leave separate 'node_modules' in each app?
                full_path = path_resolve(_configData.bench_path, file_name);
            }
            else {
                full_path = path_resolve(frappeData.path, file_name);
            }
            return `/* ${file_name} */\n` + readFileSync(full_path);  // returns output_file
        }).join("\n\n");

        // Write concatenated file to disk.
        const target_path = path_resolve(_configData.assets_path, output_file);
        writeFileSync(target_path, file_content);
        console.log(`\t${green("✔")} Created concat file 'sites/assets/${output_file}' for app 'Frappe'.`);
    });

}
