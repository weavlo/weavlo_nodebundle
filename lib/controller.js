// controller.js

// 3rd Party
import chalk from "chalk";  // https://www.npmjs.com/package/chalk
const { red, bold, magenta } = chalk;
import fs from "fs";
import { resolve as path_resolve} from "path";

// Local modules
import { bundler } from "./bundle.js";  // https://www.npmjs.com/package/rollup
import { configurator } from "./config.js";
import { concatenateFrappeFiles } from "./concatenate.js";
import { checkOrCreateAssetDirs } from "./structure.js";

export class Controller {
    constructor(_benchPath, _appName) {
        // Validate path to Bench; exit with error if invalid.
        this.targetBench = path_resolve(_benchPath);
        validateBenchPath(this.targetBench, true);
        this.targetApp = _appName;
        this.configData = new configurator(this.targetBench, this.targetApp).configData;
        console.log(magenta.bold(`Weavlo Bundler.  Mode: ${this.is_production ? "Production" : "Development"}`));
    }

    static main(_benchPath, _appName) {
        // Entry point for CLI.
        const instance = new Controller(_benchPath, _appName);
        instance._main();
    }

    _main() {
        if (this.targetApp === undefined) {
            console.log(bold("Bundling assets for all Apps.\n"));
        }
        else {
            console.log(bold(`Bundling assets for App = '${this.targetApp}'\n`));
        }

        // Step 1: Build the necessary directory structure.
        checkOrCreateAssetDirs(this.configData.bench_path);
        if (true == true) return;

        // Step 2: Concatenate certain files from the Frappe App.
        concatenateFrappeFiles.main(this.configData);
        // Create a .build file:  Why?
        // const touch = require("touch");
        // touch(path.join(sites_path, ".build"), { force: true });

        // Step 3
        if (this.targetApp) {
            bundler.bundleSingleApp(this.targetApp, this.configData);
        }
        else {
            bundler.bundleMultiApp(this.configData);
        }
    }
}

export function validateBenchPath(_path, hardFailure=false) {
    // Returns: true/false
    if(typeof _path != "string") {
        const message = "error : Expecting --benchdir argument to be a String.";
        console.log(red(message));
        if (hardFailure) { throw(message); } else { return false; }
    }

    if (fs.existsSync(_path) === false) {
        const message = `error : Path '${_path}' does not exist.`;
        if (hardFailure) { 
            throw(message);
        } 
        else {
            console.log(red(message));
            return false;
        }
    }

    if (!fs.existsSync(_path + "/apps") ||
		!fs.existsSync(_path + "/config") ||
		!fs.existsSync(_path + "/env") ||
		!fs.existsSync(_path + "/Procfile")) {
        console.log(`error : Invalid path to Bench '${_path}' : missing expected directory structure.`);
        return false;
    }

    return true;
}