// lib/config.js
import chalk  from "chalk";
const { red } = chalk;
import fs from "fs";
import path from "path";
import toml  from "toml";
import { fileURLToPath } from 'url'

// ES6 Modules
export class configurator {
    constructor(_benchPath, _app) {
        // _app is optional.  If undefined, all Apps will be scanned.
        if ( _benchPath == undefined) {
            throw "Path to bench must be a string.";
        }
        this.benchPath = _benchPath;
        this.benchPath;
        this.appName = _app;
        this.configData = this.createConfigData();
    }

    createConfigData() {
        // jStatic configuration data
        let ret = {
            bench_path : this.benchPath,
            package_manager : preferredPackageManager(),
            is_production : configurator.isProduction(),
            frappe_path : path.resolve(this.benchPath, "apps", "frappe"),
            sites_path : this.getSitesPath(),
            assets_path : path.resolve(this.getSitesPath(), "assets"),
            apps : []
        };

        let apps = this.findApps();
        for (let key in apps) {
            let appName = apps[key];
            if (this.appName && appName != this.appName) {
                continue;  // If only building configuration data for 1 app
            }
            let app = {};
            app.name = appName;
            app.repo_path = path.resolve(this.benchPath, "apps", appName);
            app.path = path.resolve(this.benchPath, "apps", appName, appName);
            app.semantic_version = configurator.getAppVersionObject(app.path);
            app.public_js_path =  path.resolve(app.path, "public/js");
            app.build_data_path = configurator.getAppBuildPath(appName, app.semantic_version);
            
            ret.apps.push(app);
        }

        return ret;
    }

    findApps() {
        const apps_txt_path =  path.resolve(this.getSitesPath(), "apps.txt");
        let appNameArray = fs.readFileSync(apps_txt_path).toString().split("\n");
        // Remove empty values, which are falsey.
        appNameArray = appNameArray.filter(item => item);
        return appNameArray;
    }

    getConfigData() {
        return this.configData;
    }

    getSitesPath() {
        return path.resolve(this.benchPath, "sites");
    }

    static dataDirPath() {
        // Returns and absolute path to the 'data' folder.  
        // Workaround for Node not supporting '__dirname' when using experimental ES6 modules.
        const relativePath = a => path.join(path.dirname(fileURLToPath(import.meta.url)), a);  // eslint-disable-line
        const pathToDataDir   = relativePath('../data');

        return pathToDataDir;
    }

    static getAppBuildPath(_appName, _semanticVersion) {
        // Given an App and Version, return a path to corresponding 'build.json' file.
        // In official repos, these are located at '../bench/apps/<app_name>/<app_name>/public/build.json'
        if (_semanticVersion == undefined) {
            throw ReferenceError(`Call to config.getAppBuildPath() without argument '_semanticVersion'`);
        }
        switch (_appName + ':v' + _semanticVersion.major) {
            case "frappe:v11":
                return path.join(configurator.dataDirPath(), "frappe_v11_build.json");
            case "frappe:v12":
                return path.join(configurator.dataDirPath(), "frappe_v12_build.json");
            case "erpnext:v11":
                return path.join(configurator.dataDirPath(), "erpnext_v11_build.json");
            case "erpnext:v12":
                return path.join(configurator.dataDirPath(), "erpnext_v12_build.json");            
            default:
                const message = `Unable to find 'build.json' for app '${_appName}', version '${_semanticVersion.major}'`;
                throw ReferenceError(message);
        }  // end switch
    }

    static getAppVersionObject(_appPath) {
        // Examine the '__init__.py' file in the Frappe folder.
        // Use regular expression matching to extract the version number.
        const initPyPath = path.join(_appPath, "__init__.py");
        const pattern = /__version__\s+=\s+(.*)/g

        const initPyData = fs.readFileSync(initPyPath).toString("utf8");
        let version_string = pattern.exec(initPyData);

        if (version_string == undefined) {
            return { 'major' : 0, 'minor' : 0, 'patch' : 0 };
        }

        version_string = version_string[1].replace(/'/g, ""); // get 2nd value, and remove single quotes.
        return parseVersionString(version_string);
    }

    static isProduction() {
        switch(process.env.FRAPPE_ENV) {
            case "production":
              return true;
              break;
            default:
              return false;
          } 
    }

    static readBuildfile_byAppVersion(app, semanticVersion) {
        // Purpose: Read 'build.json' synchronously.
        // Returns: JavaScript object
        const buildfilePath = configurator.getAppBuildPath(app, semanticVersion);
        readBuildfile_byPath(buildfilePath);
    }
}

export function readBuildfile_byPath(filePath) {
    // Purpose: Read 'build.json' synchronously.
    // Returns: JavaScript object
    try {
        const jsonString = fs.readFileSync(filePath);
        return JSON.parse(jsonString);  // Returns an Object.
    } catch(err) {
        console.log(err);
        throw err;
    }
}

function parseVersionString (str) {
    // General function for converting x.x.x to a semantic version Object.
    if (typeof(str) != 'string') {
        return false;
    }
    var x = str.split('.');
    // parse from string or default to 0 if can't parse
    var maj = parseInt(x[0]) || 0;
    var min = parseInt(x[1]) || 0;
    var pat = parseInt(x[2]) || 0;
    return {
        'major' : maj,
        'minor' : min,
        'patch' : pat
    };
}

export function preferredPackageManager() {
    // Read our global TOML file, and return the preferred Node package manager.
    // Returns: String
    const bench_config_path = "/etc/bench/config_general.toml";
    const config_toml = toml.parse(fs.readFileSync(bench_config_path, "utf-8"));
    return config_toml.package_management.javascript;
}
