// lib/structure.js

/*
    Note: Original code found in frappe/build.py
    As future ERPNext versions are released, we'll need to review, and make changes here.
*/

import { configurator } from "./config.js";
import { existsSync, mkdirSync, rmdirSync, lstatSync, unlinkSync, symlinkSync } from "fs";
import { resolve as path_resolve} from "path";

import lodash from "lodash";
import copydir from "copy-dir";

import { logger } from "./logger.js";


export function checkOrCreateAssetDirs(_benchPath) {
    // Create the necessary asset directories in a bench instance.
    const configData = new configurator(_benchPath).configData;
    const assetsPath = configData.assets_path;

    {
        logger.info("Creating asset subdirectories.").message;
        let paths = [
            path_resolve(assetsPath, "js"),  // bench/sites/assets/js
            path_resolve(assetsPath, "css")  // bench/sites/assets/css
        ];
        
        paths.forEach(path => {
            if (!existsSync(path)) {
                mkdirSync(path, { recursive : true } );
                logger.debug(`Created new path: ${path}`);
            }
        });
    }

    let symlinkList = [];  // list of key-value pairs
    const frappeData = lodash.find(configData.apps, { "name": "frappe" } );

    lodash.forEach(configData.apps, (appObject) => {
        logger.debug(`Finding symlinks for App: ${appObject.name}`);
        const appPublicPath = path_resolve(appObject.path,"public");

        // Create symlink from app's public path, to assets directory.
        if (!existsSync(appPublicPath)) {
            logger.error(`App: '${appObject.name}' does not have a subdirectory 'public'`);
            return;
        } else {
            symlinkList.push({ source: appPublicPath, 
                target: path_resolve(assetsPath, appObject.name)});
        }

        // Starting with Frappe 12, make symlinks to node_modules
        if (frappeData.semantic_version.major >= 12) {
            symlinkList.push({ source: path_resolve(appObject.repo_path, "node_modules"),
                target: path_resolve(assetsPath, appObject.name, "node_modules")});
        }

        // Determine path to 'docs'
        let app_doc_path = undefined;
        if (existsSync(path_resolve(appObject.path, "docs"))) {
            app_doc_path = path_resolve(appObject.path, "docs");
        }
        else if (existsSync(path_resolve(appObject.path, "www/docs"))) {
            app_doc_path = path_resolve(appObject.path, "www/docs");
        }
        else {
            logger.debug(`No path found to 'docs' for app '${appObject.name}'`);
        }

        if (app_doc_path) {
            symlinkList.push({ from: app_doc_path,
                to: path_resolve(assetsPath / appObject.name / "_docs")});
        }
    }); // end lodash.forEach


    if ( symlinkList.length == 0 ) {
        logger.warn("No symlinks to create.");
        return;
    }

    logger.debug(`Number of symlinks to create: ${symlinkList.length}`);
    lodash.forEach(symlinkList, (entry) => {
        createSymbolicLink(entry.source, entry.target);
    });
}


function createSymbolicLink(_source, _target, deleteExisting=false, makeCopy = false) {
    // Creates a symlink synchronously.
    const sourcePath = path_resolve(_source);
    const targetPath = path_resolve(_target);

    if ( !existsSync(sourcePath) ) {
        logger.debug(`Source ${sourcePath} does not exist.`);
    }

    if (deleteExisting && existsSync(targetPath)) {
        // Remove symbolic link or directory
        if (isPathSymbolicSync(targetPath)) {
            unlinkSync(targetPath);
        } else {            
            rmdirSync(targetPath, { recursive: true });
        }            
    }

    // If target exists, exit function.
    if (existsSync(targetPath)) {
        logger.warn("Symlink target already exists. Skipping.");
        logger.warn(`   ${targetPath}`);
        return;
    }

    if (makeCopy) {
        copydir.sync(sourcePath, targetPath);
    } else {
        try {
            symlinkSync(sourcePath, targetPath);
        }
        catch (e) {
            logger.error(`Error, cannot symlink ${sourcePath} to ${targetPath}`);
            throw(e);
        }
    }
}


function isPathSymbolicSync(_pathName) {
    // Returns true if a path is a symbolic link.
    const pathName = path_resolve(_pathName);
    const isSymbolic = lstatSync(pathName, function(err, stats) {
        if (err) { throw(err); }
        return stats.isSymbolicLink();
    });
    return isSymbolic;
}

// eslint-disable-next-line no-unused-vars
function prettyPrint(_someObject) {
    return JSON.stringify(_someObject,null,4);
}