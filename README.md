## weavlo_rollup

### Installation
```
npm install -g
```

### Usage
weavlo_rollup --bench /home/user/erpnext/mybench


### Development Notes
This project is using ES modules, not CommonJS.
https://flaviocopes.com/es-modules/
https://nodejs.org/docs/latest-v13.x/api/esm.html#esm_package_json_type_field

#### Structure
  * bin/cli.js is the initial entry point.  It utilizes Commander to parse arguments to 'bin/index.js'
  * /bin/index.js has the main program control.

The 'build.json' file here is a copy of ../apps/frappe/frappe/public/build.json

build.js is the entry point.

## Technical Documentation

### Package Dependencies
As of April 24th, 2020:
* The plugin @rollup/plugin-multi-entry has a peer dependency of rollup @1.2
* To support dynamic imports, we are using the following Bable Preset:
```
npm install --save-dev @babel/plugin-syntax-dynamic-import
```

### concatenate_frappe_files()

Scope: This function is hard-coded for the Frappe App --only--.
Purpose: Combine multiple *.js files together, to reduce number of HTTP calls.
Pseudocode:

1. In each APP's 'public' folder, there can be a file named 'build.json'
2. Some of this JSON may contain keys beginning with letters "concat", like this:
        "concat:js/moment-bundle.min.js": [
        "node_modules/moment/min/moment-with-locales.min.js",
        "node_modules/moment-timezone/builds/moment-timezone-with-data.min.js"
    ],

3. This function concatenates the child files, into a larger file with the parent node's name.
4. Results are stored in each SITE's asset folders:
    ../mybench/sites/assets/js/<concatenated_file_name>
